#!/usr/bin/env node




var argv = require('optimist')
    .usage('Usage: $0 --username [user] --password [pass] -p [port] -a [server_address]')
    .demand(['username','password'])
    .default('n','domus:')
    .default('p', 6379)
    .default('a','127.0.0.1')
    .boolean('h')
    .alias('h', 'help')
    .describe('h', 'print this help message')
    .argv;
    
if(argv.h) {
    argv.showHelp();
    process.exit(0);
}
    

var redis = require("redis");
var dbClient = redis.createClient(argv.p, argv.p, null);

dbClient.on("error", function (err) {
    console.log("Error " + err);
});
    
var user = require('./modules/user')(dbClient);
user.addUser(argv.username,argv.password, function(err, res) { dbClient.end(); });    
console.log('User '+argv.username+' added. If you wish to remove - delete directly from redis domus:passwd:<user_id>');