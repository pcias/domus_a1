/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , domusThermo = require('./routes/domusThermo')
  , domusLumen = require('./routes/domusLumen')
  , domusGatekeeper = require('./routes/domusGatekeeper')
  , domusCharter = require('./routes/domusCharter')
  //, https = require('https')
  , http = require('http')
  , path = require('path');


var optimist = require('optimist')
    .usage('Starts domus server. Requires redis running with database uploaded. See ministore2redis.js uploader\nUsage: $0  -p [redis_port] -a [redis_address]' )
    .default('p', 6379)
    .default('a','127.0.0.1')
    .boolean('h')
    .alias('h', 'help')
//    .describe('f', 'force (no questions asked - will overwrite database from files)')
    .describe('h', 'print this help message');
    
if(optimist.argv.h) {
    optimist.showHelp();
    process.exit(0);
}






//database
var redis = require("redis");
var dbClient = redis.createClient(optimist.argv.p, optimist.argv.p, null);

dbClient.on("error", function (err) {
    console.log("Error " + err);
});


//REST API routes depending on database
var hours = require('./routes/hours')(dbClient)
   , devices = require('./routes/devices')(dbClient)
   , gate = require('./routes/gate')(dbClient)
   , globals = require('./routes/globals')(dbClient)
   , logger =  require('./routes/logger')(dbClient);


var app = express();

// all environments
app.set('port', process.env.PORT || 3300);
app.set('views', __dirname + '/views');
app.set('view engine', 'hjs');
app.use(express.favicon(__dirname + '/public/images/home.ico'));
//app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.static(path.join(__dirname, 'public')));

// add timestamps in front of log messages
// since logger only returns a UTC version of date, I'm defining my own date format - using an internal module from console-stamp
express.logger.format('mydate', function() {
    var df = require('console-stamp/node_modules/dateformat');
    return df(new Date(), 'HH:MM:ss.l');
});
app.use(express.logger('[:mydate] :method :url :status :res[content-length] - :remote-addr - :response-time ms'));



// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

//user authentication
var user = require('./modules/user')(dbClient);
// Authenticated paths
    app.use(express.basicAuth(function(username, password, callback) {
        user.authenticate(username,password,callback);
    }));
app.use(app.router);






//web interface
app.get('/', routes.index);
app.get('/domusThermo', domusThermo.index);
app.get('/domusGatekeeper', domusGatekeeper.index);
app.get('/domusLumen', domusLumen.index);
app.get('/domusCharter', domusCharter.index);



//REST API
//hourly temp schedule
app.get('/hours', hours.get);
app.put('/hours/:hour', hours.put);
// //globals
app.get('/globals/:id', globals.get);
app.put('/globals/:id', globals.put);
app.post('/globals', globals.post);
//heyu devices
app.get('/devices', devices.get);
app.put('/devices/:id', devices.put);
// //gate opener
app.get('/gate', gate.open);
// //temperature logging v1 deprecated
//app.post('/logger',logger.write);
//temperature logging v2
app.post('/logger/:id', logger.logSensor);
//read log of sensotimers
app.get('/logger', logger.readLog);
//provide current time in 1970 millis
app.get('/millis', logger.millis);
//provice local time in a friendly format
app.get('/localtime', logger.localtime);

//read external temperature sensor at gate :id should be "EXTERNAL", URI will be stored at redis key "domus:sensors:EXTERNAL"
app.get('/read_sensor/:id', logger.read_sensor);



var banner = require("./routes/banner")(dbClient);
//display banner on arduino
app.get('/banner',banner.get);


//full banner for web page
app.get('/bannerfull',banner.getFull);
app.get('/weatherphoto', banner.getWeatherPhoto);

//populate banner with weather data  
app.get('/populate',banner.populate);   

// //read web api once an hour
setInterval(banner.populate,3600*1000);

    
    
var server = http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});



var loop = require('./modules/loop')(dbClient, logger);
var loopInterval = setInterval(loop.loop,14*1000);
var logLoopInterval = setInterval(loop.boilerLogLoop, 5*60*1000);  //every 5 minutes log temperatures

var mailer = require('./modules/mailer')(dbClient);


var cronJob = require('cron').CronJob;
var job;

dbClient.get('domus:globals:emailcronpattern', function(err,data) {
    if(err) {
        console.log('emailcronpattern undefined');
        return;
    } 
    job = new cronJob({
                     cronTime: JSON.parse(data),
                     onTick:     function(){
                                     console.log('emailing...');
                                     mailer.email();
                                 },
                     start: true
                     });   
    job.start();    
    mailer.email();
})



// this function is called when you want the server to die gracefully
// i.e. wait for existing connections
var gracefulShutdown = function() {
  console.log("Received kill signal, shutting down gracefully.");
  clearInterval(loopInterval);
  server.close(function() {
        console.log("Closed out client connections.");
        dbClient.quit();
        console.log("Closed out database");
        process.exit();
        });

  
   // if after 
   setTimeout(function() {
       console.error("Could not close connections in time, forcefully shutting down");
       process.exit();
  }, 5*1000);
};

// listen for TERM signal .e.g. kill 
process.on ('SIGTERM', gracefulShutdown);

// listen for INT signal e.g. Ctrl-C
process.on ('SIGINT', gracefulShutdown); 

// listen for HUP signal 
process.on ('SIGHUP', gracefulShutdown); 

