#define APPLICATION "domusKerberos - PCS "
#define VERSION " 1.1 ThingSpeak"
#include <avr/wdt.h>

#include <Bridge.h>
#include <YunServer.h>
#include <YunClient.h>

//temperature logging defines
#include <dht.h>
dht DHT;
#define DHT22_PIN 7

//



//Entry code to open the door
const char *ENTRY_CODE = ".-.";

const int switchPin = A2;  //switch at the gate
const int lockPin = 2;     //pin goin to door lock relay
const int THRSH = 800;

const unsigned long DOT_LOW = 10;     //>50ms for Morse dot 
const unsigned long DOT_HIGH = 599;   //<150ms for Morse dot
const unsigned long DASH_LOW = 1000;
const unsigned long DASH_HIGH = 3999;
const unsigned long BELL_LOW = 4000;

const int MAX_LEN = 10; //maximum number of dots/dashes to be recorded

const char *key = "6PK3SLK7LPM4B7XG";  //ThingSpeak key
const int ARRAY_SIZE = 2;

long int lastSentTS = -99999999999; //millis when last updated ThingSpeak 
long int TS_UPDATE_FREQ = 120000; //2 minutes data



YunServer server;

String last_record;

void setup() {
  Serial.begin(115200);
  // put your setup code here, to run once:
  pinMode(switchPin,INPUT);
  pinMode(lockPin, OUTPUT);
  digitalWrite(lockPin,1);
  
  Bridge.begin();  
  server.listenOnLocalhost();
  server.begin();

  
  //watchdog - in case we hanged or someone was Morsing too long
  wdt_enable(WDTO_8S);
}

//records up to 20 dots/dashes (Morse Code) not longer than MAX_DUR seconds
//beware - global watchdog is reset only in the main loop - this fuction is time limited by this
//returns : 
// 0 : wrong code
// 1 : correct code
// 2 : bell


int recording() {
  char reading[MAX_LEN];
  unsigned long pressDur;
  unsigned long pressStart; 
  int i;
    
  if(analogRead(switchPin) > THRSH) {
    
    Serial.println("Recording start...");
    
    for(i=0; i < MAX_LEN - 1;) {
      pressStart = millis(); 
      while(analogRead(switchPin) > THRSH) {
          pressDur = millis() - pressStart;
          if(pressDur > BELL_LOW) {   //ok, this is too long, bell ringing, exit
              Serial.println("BELL");
              return 2;  //bell ringing
          }
      }    
//      Serial.print("P");
//      Serial.println(pressDur);
    
      if(pressDur > DOT_LOW && pressDur < DOT_HIGH) {
        reading[i++]='.';
        Serial.print(".");
      }  
    
      if(pressDur > DASH_LOW && pressDur < DASH_HIGH) {
        reading[i++]='-';  
        Serial.print("-");
      }  

      pressDur = 0;
      pressStart = millis();
      while(analogRead(switchPin) < THRSH && pressDur < 1100) {
          pressDur = millis() - pressStart;
      }    
//      Serial.print("U");
//      Serial.println(pressDur);
      
      if(pressDur >  1000)  {    //if unpressed for > 2s - end of code assumed
        Serial.println("code finished");
        break;
      }  
      
    }
    //add terminator to the end of reading
    reading[i]=0;
    last_record = reading;
    
    Serial.print("Reading:");
    Serial.println(reading);
    //compare reading with the ENTRY_CODE
    for(i=0; i < MAX_LEN && reading[i] && ENTRY_CODE[i] ; i++)
     if(reading[i] != ENTRY_CODE[i])
          return 0;      //immediate exit
    if(ENTRY_CODE[i])  //did not get till the end of the ENTRY_CODE     
        return 0;
        
    return 1; //ok, survived comparison, return success (correct code)
        
  }  
}


void loop() {
  Serial.println("Booted");
  
  //web service
  YunClient client = server.accept();
  if(client) {
    //read the command
    String command = client.readString();
    command.trim();
    Serial.println("URI:"+command);
    if(command=="sezame") {
          wdt_reset();
          client.print("{\"status\" : \"OPEN\"");
          client.print(", \"switch\" : ");
          client.print(analogRead(switchPin));
          client.print(",\"last_record\" : \"");
          client.print(last_record);
          client.print("\"}");
          client.stop();
          wdt_reset();
          digitalWrite(lockPin,0);
          delay(6000);
          digitalWrite(lockPin,1);

    }
    if(command=="read_sensor") {
        int chk = DHT.read22(DHT22_PIN);
        //reading temperature and humidity from the attached DHT22
        client.print("{\"sensor_id\" : \"DHT22\"");
        switch (chk)
          {
            case DHTLIB_OK:  
                      client.print(", \"status\" : \"OK\""); 
                      break;
            case DHTLIB_ERROR_CHECKSUM: 
                      Serial.print(", \"status\" : \"CHKSUM_ERROR\""); 
                      break;
            case DHTLIB_ERROR_TIMEOUT: 
                      Serial.print(", \"status\" : \"TIMEOUT_ERROR\""); 
                      break;
            default: 
                      (", status : \"UNKN_ERROR\""); 
                      break;
          }
        client.print(", \"temperature\" : ");
        client.print(DHT.temperature);  
        client.print(", \"humidity\" : ");
        client.print(DHT.humidity);  
        client.print(" }");
        client.stop();
    }
    else {
          client.print("{\"status\" : \"CLOSED\"");
          client.print(", \"switch\" : ");
          client.print(analogRead(switchPin));
          client.print(", \"last_record\" : \"");
          client.print(last_record);
          client.print("\"}");
          client.stop();
    }      
  }
  
  //press button 'morse code' handling below
  if(analogRead(switchPin) > THRSH) 
    switch (recording()) { 
      case 0:  
          digitalWrite(lockPin,1);
          Serial.println("Wrong code");
          break;
      case 1:    
          digitalWrite(lockPin,0);
          Serial.println("Code accepted");
          wdt_reset();
          delay(6000);
          digitalWrite(lockPin,1);
          break;
      case 2:
          Serial.println("Bell ringing");   
          break;
    }
  delay(50);
  wdt_reset();  
  
  if(millis() - lastSentTS > TS_UPDATE_FREQ) {
    float data[ARRAY_SIZE];
    int chk = DHT.read22(DHT22_PIN);
    
    if (chk == DHTLIB_OK )
      {
        data[0] = (float)DHT.temperature;
        data[1] = (float)DHT.humidity;
        postToThingSpeak(key, data); 
        lastSentTS = millis();
      }
  }
}


void postToThingSpeak(String key, float value[]) {
  Process p;
  String cmd = "curl --data \"key="+key;
  for (int i=0;i<ARRAY_SIZE;i++) {
    cmd = cmd + "&field"+ (i+1) + "=" + value[i];
  }
  cmd = cmd + "\" http://api.thingspeak.com/update";
  p.runShellCommand(cmd);
  Console.println(cmd);
  p.close();
}