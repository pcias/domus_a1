
//     URL: http://arduino.cc/playground/Main/DHTLib
//
// Released to the public domain
//

#include <dht.h>
#include <Process.h>

#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>
//backlight pin
#define BLPIN 11
// Software SPI (slower updates, more flexible pin options):
// pin 7 - Serial clock out (SCLK)
// pin 6 - Serial data out (DIN)
// pin 5 - Data/Command select (D/C)
// pin 4 - LCD chip select (CS)
// pin 3 - LCD reset (RST)
Adafruit_PCD8544 display = Adafruit_PCD8544(7, 6, 5, 4, 3);



#define LOGFREQ 3600000

#define ACTFREQ 10100
#define USERNAME "sensor"
#define PASSWORD "sensor1data"

//#define DOMUS_R2_URL "https://domus_r2-c9-pcias.c9.io"
//#define DOMUS_R2_URL "http://pcsbotaniczna.no-ip.org:3000"
#define DOMUS_R2_URL "http://192.168.1.126:3300"
#define SENSOR_ID "YUN2_DHT22"


dht DHT;
#define DHT22_PIN 3


bool ok = false;

float extTemp, extHum, insTemp;  //external temeprature
String displayStr, localTime;




void setup()
{
  // Initialize Bridge
  Bridge.begin();


  
  //display setup
  pinMode(BLPIN,OUTPUT);
  display.begin();
  display.setContrast(50);
  display.display();
  display.clearDisplay();   // clears the screen and buffer
  
  analogWrite(BLPIN,20);
  // Wait until a Serial Monitor is connected.
  //while (!Serial);

  Serial.println("DHT INTERFACE FOR DOMUS THERMO ");
  Serial.print("LIBRARY VERSION: ");
  Serial.println(DHT_LIB_VERSION);
  Serial.println();
  Serial.println("Type,\tstatus,\tHumidity (%),\tTemperature (C)");
  
  
}

void logTemp(double temp, double humid) {
//  Process p;
//  p.begin("curl");
//  p.addParameter("-u"); 
//  p.addParameter(String(USERNAME)+":"+PASSWORD);
//  p.addParameter("-k");
//  p.addParameter("--data");
//  p.addParameter(String("sensor_id=")+SENSOR_ID+"&temperature="+temp+"&humidity="+humid);
//  //p.addParameter(String("sensor_id=")+"comment&actual_temperature=23&manual_temperature=20&temperature_source=MAN");
//  p.addParameter(String(DOMUS_R2_URL)+"/logger");
//  p.run();
}




void loop()
{
  long int m;
  m = millis();
 
    int i;
  
  
  if(!(m % ACTFREQ)) {
    
    refreshExtTemperature();
    getCond();
    getTime();
//    getBanner();
    show();
    // READ DATA
//    Serial.print("DHT22, \t");
    int chk;// = DHT.read22(DHT22_PIN);
    switch (chk)
    {
      case DHTLIB_OK:  
                Serial.print("OK,\t"); 
                ok = true;
                break;
      case DHTLIB_ERROR_CHECKSUM: 
                Serial.print("Checksum error,\t"); 
                ok = false;
                break;
      case DHTLIB_ERROR_TIMEOUT: 
                Serial.print("Time out error,\t");
                ok = false; 
                break;
      default: 
                Serial.print("Unknown error,\t"); 
                ok = false;
                break;
    }
    // DISPLAY DATA
//    Serial.print(DHT.humidity, 1);
//    Serial.print(",\t");
//    Serial.println(DHT.temperature, 1);
  
  }  
  
  //every ca 5 minutes log temperature
//  if(!(m % LOGFREQ) && ok) {
//    logTemp(DHT.temperature, DHT.humidity);
//  }
  

}

void getCond() {
  Process p;
  String status;

  
  p.begin("curl");
  p.addParameter("-u"); 
  p.addParameter(String(USERNAME)+":"+PASSWORD);
  p.addParameter("-k");
  p.addParameter(String(DOMUS_R2_URL)+"/globals/ext_temperature");
  p.run();
  status = "";
  int i = 0;
  while (p.available()>0 && i < 7) {
    char c = p.read();
    if(c!='\"' && c!='\\') 
      status+=c;  
    i++;
  }
  extTemp = status.toFloat();
  
  p.begin("curl");
  p.addParameter("-u"); 
  p.addParameter(String(USERNAME)+":"+PASSWORD);
  p.addParameter("-k");
  p.addParameter(String(DOMUS_R2_URL)+"/globals/ext_humidity");
  p.run();
  status = "";
  while (p.available()>0 && i < 7) {
    char c = p.read();
    if(c!='\"' && c!='\\') 
      status+=c;  
    i++;
  }
  extHum = status.toFloat();

//  p.begin("curl");
//  p.addParameter("-u"); 
//  p.addParameter(String(USERNAME)+":"+PASSWORD);
//  p.addParameter("-k");
//  p.addParameter(String(DOMUS_R2_URL)+"/globals/actual_temperature");
//  p.run();
//  status = "";
//  while (p.available()>0 && i < 7) {
//    char c = p.read();
//    if(c!='\"' && c!='\\') 
//      status+=c;  
//    i++;
//  }
//  insTemp = status.toFloat();

  
}

void refreshExtTemperature() {
  Process p;
  p.begin("curl");
  p.addParameter("-u"); 
  p.addParameter(String(USERNAME)+":"+PASSWORD);
  p.addParameter("-k");
  p.addParameter(String(DOMUS_R2_URL)+"/read_sensor/EXTERNAL");
  p.run();
}

void getBanner() {
  Process p;
  p.begin("curl");
  p.addParameter("-u"); 
  p.addParameter(String(USERNAME)+":"+PASSWORD);
  p.addParameter("-k");
  p.addParameter(String(DOMUS_R2_URL)+"/banner");
  p.run();
  // Print command output on the Serial.
  // A process output can be read with the stream methods
  displayStr = "";
  int i=0;
  while (p.available()>0 && i < 28) {
    char c = p.read();
    if(c!='\"' && c!='\\') 
      displayStr+=c;  
    i++;
  }
}

void getTime() {
  Process p;
  
  p.begin("curl");
  p.addParameter("-u"); 
  p.addParameter(String(USERNAME)+":"+PASSWORD);
  p.addParameter("-k");
  p.addParameter(String(DOMUS_R2_URL)+"/localtime");
  p.run();
  // Print command output on the Serial.
  // A process output can be read with the stream methods
  localTime = "";
  int i=0, semicolons=0;
  while (p.available()>0 && i < 14 && semicolons < 2) {
    char c = p.read();
    if(c==':')
      semicolons++;
    if(c!='\"' && c!='\\' && semicolons < 2) 
      localTime+=c;  
    i++;
  }
  
  //lighten up unless at night
  if(localTime > "6:00" || localTime < "23:00") {
    analogWrite(BLPIN,20);
  }
  else {
    analogWrite(BLPIN,0);
  }
  
    
}

void show() {
  //Serial.println("display");
  display.clearDisplay();
  display.setTextSize(2);
  display.setTextColor(BLACK);
  display.setCursor(0,0);
  display.println(String(extTemp)+"C");
  display.setTextSize(1);
  display.println(String(extHum)+"%");
  display.setTextSize(2);
  display.print(localTime);
  
 
  //display.println(displayStr);
  //display.println(String("inside:")+insTemp);
  display.display();  
}