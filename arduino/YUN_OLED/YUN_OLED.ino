                                  
//This one is for temeprature display using 4 wire I2C 128x64 display based on SSD1306
//Wiring:
//  YUN   ->  OLED I2C
//  2     ->  SDA
//  3     ->  SCL
//  3.3V  ->  VCC
//  GND   ->  GND

//  YUN   ->  DHT22
//  5V    ->  VCC
//  7     ->  BUS (one-wire)
//  GND   ->  GND




#include <dht.h>
#include <Process.h>
#include "U8glib.h"

U8GLIB_SSD1306_128X64 u8g(U8G_I2C_OPT_NONE);	// I2C / TWI


#define LOGFREQ 3600000

#define ACTFREQ 10100
#define USERNAME "sensor"
#define PASSWORD "sensor1data"

#define DOMUS_R2_URL "http://192.168.1.126:3300"
#define SENSOR_ID "YUN2_DHT22"


dht DHT;
#define DHT22_PIN 7


bool ok = false;

float extTemp, extHum, insTemp;  //external temeprature
String displayStr, localTime;




void setup()
{
  // Initialize Bridge
  Bridge.begin();

 // assign default color value
  if ( u8g.getMode() == U8G_MODE_R3G3B2 ) {
    u8g.setColorIndex(255);     // white
  }
//  else if ( u8g.getMode() == U8G_MODE_GRAY2BIT ) {
//    u8g.setColorIndex(3);         // max intensity
//  }
//  else if ( u8g.getMode() == U8G_MODE_BW ) {
//    u8g.setColorIndex(1);         // pixel on
//  }
//  else if ( u8g.getMode() == U8G_MODE_HICOLOR ) {
//    u8g.setHiColorByRGB(255,255,255);
//  }
  //while (!Serial);  
}

//void logTemp(double temp, double humid) {
//  Process p;
//  p.begin("curl");
//  p.addParameter("-u"); 
//  p.addParameter(String(USERNAME)+":"+PASSWORD);
//  p.addParameter("-k");
//  p.addParameter("--data");
//  p.addParameter(String("sensor_id=")+SENSOR_ID+"&temperature="+temp+"&humidity="+humid);
//  //p.addParameter(String("sensor_id=")+"comment&actual_temperature=23&manual_temperature=20&temperature_source=MAN");
//  p.addParameter(String(DOMUS_R2_URL)+"/logger");
//  p.run();
//}




void loop()
{
  long int m;
  m = millis();
 
    int i;
  
  
  if(!(m % ACTFREQ)) {
    
    refreshExtTemperature();
    getCond();
    getTime();
//    getBanner();
    // picture loop
    u8g.firstPage();  
    do {
      draw();
    } while( u8g.nextPage() );
    // READ DATA
//    Serial.print("DHT22, \t");
    int chk;// = DHT.read22(DHT22_PIN);
    switch (chk)
    {
      case DHTLIB_OK:  
                Serial.print("OK,\t"); 
                ok = true;
                break;
      case DHTLIB_ERROR_CHECKSUM: 
                Serial.print("Checksum error,\t"); 
                ok = false;
                break;
      case DHTLIB_ERROR_TIMEOUT: 
                Serial.print("Time out error,\t");
                ok = false; 
                break;
      default: 
                Serial.print("Unknown error,\t"); 
                ok = false;
                break;
    }
    
    
    // DISPLAY DATA
//    Serial.print(DHT.humidity, 1);
//    Serial.print(",\t");
//    Serial.println(DHT.temperature, 1);
  
  }  
  
  //every ca 5 minutes log temperature
//  if(!(m % LOGFREQ) && ok) {
//    logTemp(DHT.temperature, DHT.humidity);
//  }
  

}

void getCond() {
  Process p;
  String status;

  
  p.begin("curl");
  p.addParameter("-u"); 
  p.addParameter(String(USERNAME)+":"+PASSWORD);
  p.addParameter("-k");
  p.addParameter(String(DOMUS_R2_URL)+"/globals/ext_temperature");
  p.run();
  status = "";
  int i = 0;
  while (p.available()>0 && i < 7) {
    char c = p.read();
    if(c!='\"' && c!='\\') 
      status+=c;  
    i++;
  }
  extTemp = status.toFloat();
  
  p.begin("curl");
  p.addParameter("-u"); 
  p.addParameter(String(USERNAME)+":"+PASSWORD);
  p.addParameter("-k");
  p.addParameter(String(DOMUS_R2_URL)+"/globals/ext_humidity");
  p.run();
  status = "";
  while (p.available()>0 && i < 7) {
    char c = p.read();
    if(c!='\"' && c!='\\') 
      status+=c;  
    i++;
  }
  extHum = status.toFloat();

//  p.begin("curl");
//  p.addParameter("-u"); 
//  p.addParameter(String(USERNAME)+":"+PASSWORD);
//  p.addParameter("-k");
//  p.addParameter(String(DOMUS_R2_URL)+"/globals/actual_temperature");
//  p.run();
//  status = "";
//  while (p.available()>0 && i < 7) {
//    char c = p.read();
//    if(c!='\"' && c!='\\') 
//      status+=c;  
//    i++;
//  }
//  insTemp = status.toFloat();

  
}

void refreshExtTemperature() {
  Process p;
  p.begin("curl");
  p.addParameter("-u"); 
  p.addParameter(String(USERNAME)+":"+PASSWORD);
  p.addParameter("-k");
  p.addParameter(String(DOMUS_R2_URL)+"/read_sensor/EXTERNAL");
  p.run();
}

//void getBanner() {
//  Process p;
//  p.begin("curl");
//  p.addParameter("-u"); 
//  p.addParameter(String(USERNAME)+":"+PASSWORD);
//  p.addParameter("-k");
//  p.addParameter(String(DOMUS_R2_URL)+"/banner");
//  p.run();
//  // Print command output on the Serial.
//  // A process output can be read with the stream methods
//  displayStr = "";
//  int i=0;
//  while (p.available()>0 && i < 28) {
//    char c = p.read();
//    if(c!='\"' && c!='\\') 
//      displayStr+=c;  
//    i++;
//  }
//}

void getTime() {
  Process p;
  
  p.begin("curl");
  p.addParameter("-u"); 
  p.addParameter(String(USERNAME)+":"+PASSWORD);
  p.addParameter("-k");
  p.addParameter(String(DOMUS_R2_URL)+"/localtime");
  p.run();
  // Print command output on the Serial.
  // A process output can be read with the stream methods
  localTime = "";
  int i=0, semicolons=0;
  while (p.available()>0 && i < 14 && semicolons < 2) {
    char c = p.read();
    if(c==':')
      semicolons++;
    if(c!='\"' && c!='\\' && semicolons < 2) 
      localTime+=c;  
    i++;
  }
  
  
    
}


void draw(void) {
  
  u8g.setFont(u8g_font_helvR18r);
  u8g.setPrintPos(0,24);
  u8g.print( String(extTemp)+"C");
  u8g.setFont(u8g_font_helvR14r);
  u8g.setPrintPos(0,42);
  u8g.print(String(extHum)+"%");
  u8g.setFont(u8g_font_helvR18r);
  u8g.setPrintPos(0,64);
  u8g.print(localTime);
}

