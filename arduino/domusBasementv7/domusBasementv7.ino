//DEPRECATED!!!

/*
domus boilerServer

v5: new watchdog - every 8 second  reset 
v6: added Dallas Temperature Sensor on PIN 3; IP interface now returns meaningful data ('RET: protocol')
v7 2014.03.30 added water sensing
v9 change command language, added thermostatic function for boiler
*/

/* PIN USAGE
D3 - Dallas Temp Sensor

D5 - A (Global) relay 
D6 - B (Pump) relay

D2 - Ethernet interrupt (optional with solder bridge "INT")
D4 - SD SPI CS
D10 - Ethernet SPI CS
D11 - Not connected (but should be SPI MOSI)
D12 - Not connected (but should be SPI MISO)
D13 - SPI SCK
A0 - SD Write Protect
A1 - SD Detect
A2 - water sensing (A2->2.2kOhm resistor->5v; A2->water; water->GND ) 
*/

#include <SPI.h>
#include <Ethernet.h>
#include <avr/wdt.h>
#include <MemoryFree.h>

#include <OneWire.h>
#include <DallasTemperature.h>

#define APPLICATION "domusBasement - PCS "
#define VERSION " 9.0 "


// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:
byte mac[] = { 
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress ip(192,168,1,7);
const int PORT = 3000; 

//const int GLOBAL_PIN = 5;
const int A_PIN =5;

//const int PUMP_PIN = 6;
const int B_PIN = 6;

const int WATER_SENS_PIN = A2;
const int DRY = 950;

int target = 70; //target temperature for boiler
float actual = 20.0;
const float hysteresis = 3.0;

// Dallas Temperature Sensor data wire is plugged into PIN 2 on the Arduino
#define ONE_WIRE_BUS 3

// Initialize the Ethernet server library
// with the IP address and port you want to use 
EthernetServer server(PORT);

// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);

// Pass our oneWire reference to Dallas Temperature. 
DallasTemperature sensors(&oneWire);

void setup() {
 // Open serial communications and wait for port to open:
  Serial.begin(9600);
   while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }
  
  sensors.begin();
  
  pinMode(A_PIN,OUTPUT);
  pinMode(B_PIN,OUTPUT);
  
  
  
  // start the Ethernet connection and the server:
  Ethernet.begin(mac, ip);
  server.begin();
  Serial.print(APPLICATION);
  Serial.print(VERSION);
  Serial.print("RESET: server is at ");
  Serial.print(Ethernet.localIP());
  Serial.print(":");
  Serial.println(PORT);

  
  
  //client has max 4s to complete command and close connection
  //wdt_enable(WDTO_4S);

  //reset every 8 seconds unless packet was received = wiznet works
  wdt_enable(WDTO_8S);
}

long int secs = 0;

void loop() {
  
//actuate thermostat
//BULLSHIT THERMOSTAT WILL NOT WORK FOR SOMETHING THAT INSTABLE
  long int time = millis();
  if(time >= secs) {
    secs = time + 5000;
    //Serial.println(secs);
    sensors.requestTemperatures(); // Send the command to get temperatures
    actual = sensors.getTempCByIndex(0);  //baniak
    if(actual < target - hysteresis) {
      digitalWrite(A_PIN,LOW);
    }
    
    //actuate thermostat
    if(actual > target + hysteresis) {
      digitalWrite(A_PIN,HIGH);
    }
    //wdt_reset();
  }  
  
  
  // listen for incoming clients
  EthernetClient client = server.available();
  if (client) {
    String linecom;
    String retval;

    Serial.println("new client");
    // an request ends with a blank line
    boolean currentLineIsBlank = true;
    
    while (client.connected()) {
      if (client.available()) {
        char c = client.read();
        if(c=='\n') {
          //client has 8 seconds to send command
          wdt_reset();
          executeCommand(linecom, retval);
          client.print("RET:");
          client.println(retval);
          //client.print("freeMemory()=");
          //client.println(freeMemory());
          linecom = "";   
          retval ="";    
        }
        else  
          linecom+=c;       
          //Serial.write(c);
        }
      }

    // give client time to receive the data
    delay(1);
    // close the connection:
    client.stop();
    Serial.println("client disconnected");
  }
}


void executeCommand(String command, String &retval) {
  Serial.print("command: ");
  Serial.println(command);
  
  //global on
  if(command=="DOA1") { 
    digitalWrite(A_PIN,LOW);  //A was global
    retval = String("A_ON");
  }
  //global off
  if(command=="DOA0") { 
    digitalWrite(A_PIN,HIGH);
    retval = String("A_OFF");
  }
  //pump on
  if(command=="DOB1") {  //B was PUMP
    digitalWrite(B_PIN,LOW);
    retval = String("B_ON");
  }
  //pump off
  if(command=="DOB0") {
    digitalWrite(B_PIN,HIGH);
    retval = String("B_OFF");
  }
  //read temperature from boiler
  if(command=="GETEMP") {  //baniak
    //sensors.requestTemperatures(); // Send the command to get temperatures
    retval = String(sensors.getTempCByIndex(0));  //baniak
  }
  if(command=="GEACT") {  //baniak
    retval = String(actual);  //baniak
  }
  //read temperature from A pump
  if(command=="GETA") {
    //sensors.requestTemperatures(); // Send the command to get temperatures
    retval = String(sensors.getTempCByIndex(1));  //A pump
  }
  //read temperature from B pump
  if(command=="GETB") {
    //sensors.requestTemperatures(); // Send the command to get temperatures
    retval = String(sensors.getTempCByIndex(2));  //B pump
  }
  //set target for baniak
  if(command.startsWith("SETEMP")) {
    if(int _t = command.substring(6).toInt()) {
      target = _t;
    }
    retval = String(target);
  }
  //get target
  if(command=="GESET") {
    retval = String(target);
  }
  
  //read wet sensor
  if(command=="GEWET") {
  //sense water (szambo)
    int moisture = analogRead(WATER_SENS_PIN);
    if(moisture > DRY) 
      retval = String("DRY");
    else
      retval = String("WET");   
  }
  //get free memory
  if(command=="GEFREE") {
    retval =  String(freeMemory()); 
  }  
  Serial.print("freeMemory()=");
  Serial.println(freeMemory());
}
