#include <LiquidCrystal.h>
#include <Process.h>

//will need to add a watchdog here

//application data
#define WEBTEMP 0
#define MANTEMP 1
#define USUTEMP 2

#define MINTEMP 10
#define MAXTEMP 30

#define LOGFREQ 600000
#define ACTFREQ 4100
long lastlog = -999999999; 


#define USERNAME "zaba"
#define PASSWORD "mala"

//#define DOMUS_R2_URL "https://domus_r2-c9-pcias.c9.io"
//#define DOMUS_R2_URL "http://pcsbotaniczna.no-ip.org:3000"
//#define DOMUS_R2_URL "http://95.85.2.233:8080"
#define DOMUS_R2_URL "http://192.168.1.126:3300"
#define GATE_URL "http://192.168.1.140/arduino/sezame"
#define SENSOR_ID "YUN_LCD_Controller"

volatile int manTemp=20;  //temperature set from com or by buttons 
volatile int setman = 0;  //manually set by button recently

int webTemp;  //web temp setting
float extTemp;  //external temeprature
float boilerTemp; //water boiler temperature
String displayStr;
volatile int tempSource = WEBTEMP;   //0 WEB (timers) -> 1 MAN (manual) -> 2 USU (usual temperature this hour and day of week)

volatile int gateToOpen = 0;

const char *key = "KEMCEMGGH1PHJB29";  //ThingSpeak key


LiquidCrystal lcd(8, 9, 4, 5, 6, 7); //konfigurowanie linii do których został dołączony LCD

const int Led1 = 13; //przypisanie aliasów do pinów portów
const int Led2 = 12; 
const int Led3 = 11; 
const int Led4 = 10; 
const int SW1 = 3; 
const int SW2 = 2; 
const int SW3 = 1; 
const int SW4 = 0; 
const int Buzzer = A5; 


void setup() {
  Bridge.begin();
  lcd.begin(16,2);
  //lcd.autoscroll();
  lcd.setCursor(0,0);
  pinMode(Led1, OUTPUT); //konfigurowanie I/O, do których są
  //dołączone diody LED
  pinMode(Led2, OUTPUT);
  pinMode(Led3, OUTPUT);
  pinMode(Led4, OUTPUT);
  pinMode(Buzzer, OUTPUT); //konfigurowanie I/O, do której jest
  //dołączony brzęczyk piezzo
  pinMode(SW1, INPUT); //konfigurowanie I/O, do których są
  //dołączone przyciski
  pinMode(SW2, INPUT);
  pinMode(SW3, INPUT);
  pinMode(SW4, INPUT);
 
  digitalWrite(Led1, HIGH);
  digitalWrite(Led2, HIGH);
  digitalWrite(Led3, HIGH);
  digitalWrite(Led4, HIGH);
  
  digitalWrite(Buzzer, HIGH);
  
  // Open serial communications and wait for port to open:
  //Serial.begin(9600);
  // while (!Serial) {
  //  ; // wait for serial port to connect. Needed for Leonardo only
  //}  
  
  
  
//  attachInterrupt(0, man_minus, FALLING); //pin 2 = SW2
//  attachInterrupt(1, man_plus, FALLING);  //pin 3 = SW1
//attachInterrupt(3, web_man, FALLING);  //pin 1 = SW3 //does not work
  attachInterrupt(0, markGateToOpen, FALLING);  //pin 2 = SW2 
}


String linecom;

void loop() {  
  //send commands up/temp
  long int m;
  m = millis();
  
  if(!(m % ACTFREQ)) {
      dispTemp();
      getBanner();
      sendTemp();
      getWebTemperature();
      getExtTemperature();
      getHeating();
      getBoilerStatus();
      getBoilerTemperature();

      dispStr();
  }  
  
  //every ca 5 minutes log temperature
  //and request refresh of ext_temperature
  if(m - lastlog > LOGFREQ) {
    logTemp(temperature(), 0);
    refreshExtTemperature();
    lastlog = m;
  }
  
  if(gateToOpen)
    openGate();
  
  if(!(m%100)) {
//    if(digitalRead(SW1) == LOW) {
//      manTemp>MINTEMP? manTemp-- : 10;
//      //delay(50);
//      tempSource = MANTEMP;
//      //sendTempSource();
//      dispTemp();
//    }
//    if(digitalRead(SW2) == LOW) {
//      manTemp<MAXTEMP? manTemp++ : 30;
//      tempSource = MANTEMP;
//      //sendTempSource();
//      dispTemp();
//    }    
    if(digitalRead(SW4) == LOW) {
//      displayStr = "Will be away...";
//      dispStr();
//      delay(100);
    }    
  }
}





void markGateToOpen() {
  digitalWrite(Led2, LOW);
  gateToOpen = 1;
  //digitalWrite(LED1, HIGH);
}

void openGate() {
  gateToOpen = 0;
  Process p;
  displayStr = "Gate Open";
  dispStr();
  p.begin("curl");
  //p.addParameter("-u"); 
  //p.addParameter(String(USERNAME)+":"+PASSWORD);
  p.addParameter("-k");

  p.addParameter(GATE_URL);
  p.run();
  digitalWrite(Led2, HIGH);
}



void dispTemp() {
    lcd.setCursor(0,1);

    lcd.print("T");      //target temp
    lcd.print("    ");
    lcd.setCursor(1,1);
    lcd.print(webTemp);
    
    lcd.setCursor(3,1);
    lcd.print("I");       //inside temp
    lcd.print("   ");
    lcd.setCursor(4,1);
    lcd.print((int)temperature());
    
    lcd.setCursor(6,1);
    lcd.print("O");       //outside temp
    lcd.print("   ");
    lcd.setCursor(7,1);
    lcd.print(extTemp);
    
    lcd.setCursor(11,1);
    lcd.print("B");       //boiler temp
    lcd.print("   ");
    lcd.setCursor(12,1);
    lcd.print(boilerTemp);
    
}


void dispStr() {
  int i;
  int len;
  
  len = displayStr.length();
  
  lcd.setCursor(0,0);
  lcd.print("                ");
  lcd.setCursor(0,0);
  lcd.print(displayStr);
  delay(1000);
  if(len > 16) {
    lcd.setCursor(0,0);
    lcd.print(displayStr.substring(len-16));
    delay(1000);
  }
  lcd.setCursor(0,0);
  lcd.print("                ");
  lcd.setCursor(0,0);
  lcd.print(displayStr);

//  for(i = 0 ; i < len-16 ; i++) {
//    lcd.setCursor(0,0);
//    lcd.print(displayStr.substring(i));
//    delay(100);
//  }
//  for(i = len-16 ; i >= 0  ; i--) {
//    lcd.setCursor(0,0);
//    lcd.print(displayStr.substring(i));
//    delay(100);
//  }
}

void getBanner() {
  Process p;
  //digitalWrite(Led2, LOW);
  p.begin("curl");
  p.addParameter("-u"); 
  p.addParameter(String(USERNAME)+":"+PASSWORD);
  p.addParameter("-k");
  p.addParameter(String(DOMUS_R2_URL)+"/banner");
  p.run();
  // Print command output on the Serial.
  // A process output can be read with the stream methods
  displayStr = "";
  int i=0;
  while (p.available()>0 && i < 32) {
    char c = p.read();
    if(c!='\"' && c!='\\') 
      displayStr+=c;  
    i++;
  }
  //digitalWrite(Led2, HIGH);  
}

void getBoilerStatus() {
  Process p;
  String status;
  
  //digitalWrite(Led2, LOW);
  p.begin("curl");
  p.addParameter("-u"); 
  p.addParameter(String(USERNAME)+":"+PASSWORD);
  p.addParameter("-k");
  p.addParameter(String(DOMUS_R2_URL)+"/globals/boiler_connection_ok");
  p.run();
  int i  = 0;
  while (p.available()>0 && i < 1) {
    char c = p.read();
    if(c!='\"' && c!='\\') 
      status+=c;  
    i++;
  }
  
  if(status[0]=='f') {
    digitalWrite(Buzzer, LOW);
    digitalWrite(Led4, LOW);
    delay(5);
    digitalWrite(Buzzer, HIGH);
  }
  else {
    digitalWrite(Led4, HIGH);
  }
  
}

void getHeating() {
  Process p;
  String status;
  
  
  p.begin("curl");
  p.addParameter("-u"); 
  p.addParameter(String(USERNAME)+":"+PASSWORD);
  p.addParameter("-k");
  p.addParameter(String(DOMUS_R2_URL)+"/globals/heating");
  p.run();
  int i  = 0;
  while (p.available()>0 && i < 5) {
    char c = p.read();
    if(c!='\"' && c!='\\') 
      status+=c;  
    i++;
  }
  
  //debug
  //displayStr = status;
  
  if(status[0]=='t') 
    digitalWrite(Led3, LOW);
  else 
    digitalWrite(Led3, HIGH);
}


void getExtTemperature() {
  Process p;
  String status;
  
  digitalWrite(Led2, LOW);
  p.begin("curl");
  p.addParameter("-u"); 
  p.addParameter(String(USERNAME)+":"+PASSWORD);
  p.addParameter("-k");
  p.addParameter(String(DOMUS_R2_URL)+"/globals/ext_temperature");
  p.run();
  status = "";
  int i = 0;
  while (p.available()>0 && i < 7) {
    char c = p.read();
    if(c!='\"' && c!='\\') 
      status+=c;  
    i++;
  }
  extTemp = status.toFloat();
  digitalWrite(Led2, HIGH);
}

void getBoilerTemperature() {
  Process p;
  String status;
  
  digitalWrite(Led2, LOW);
  p.begin("curl");
  p.addParameter("-u"); 
  p.addParameter(String(USERNAME)+":"+PASSWORD);
  p.addParameter("-k");
  p.addParameter(String(DOMUS_R2_URL)+"/globals/waterTemp");
  p.run();
  status = "";
  int i = 0;
  while (p.available()>0 && i < 7) {
    char c = p.read();
    if(c!='\"' && c!='\\') 
      status+=c;  
    i++;
  }
  boilerTemp = status.toFloat();
  digitalWrite(Led2, HIGH);
}

void getWebTemperature() {
  Process p;
  String status;
  
  digitalWrite(Led2, LOW);
  p.begin("curl");
  p.addParameter("-u"); 
  p.addParameter(String(USERNAME)+":"+PASSWORD);
  p.addParameter("-k");
  p.addParameter(String(DOMUS_R2_URL)+"/globals/web_temperature");
  p.run();
  status = "";
  int i = 0;
  while (p.available()>0 && i < 5) {
    char c = p.read();
    if(c!='\"' && c!='\\') 
      status+=c;  
    i++;
  }
  webTemp = status.toInt();
  digitalWrite(Led2, HIGH);
}


void logTemp(double temp, double humid) {
  Process p;
  p.begin("curl");
  p.addParameter("-u"); 
  p.addParameter(String(USERNAME)+":"+PASSWORD);
  p.addParameter("-k");
  //p.addParameter("-X POST");
  p.addParameter("--data");
  p.addParameter(String("sensor_id=")+SENSOR_ID+"&temperature="+temp+"&humidity="+humid);
  //p.addParameter(String("sensor_id=")+"comment&actual_temperature=23&manual_temperature=20&temperature_source=MAN");
  p.addParameter(String(DOMUS_R2_URL)+"/logger/LCD");
  p.run();
  
  //and now ThingSpeak
  p.begin("curl");
  p.addParameter("--data");
  p.addParameter(String("key=") + key + "&" + "field1=" + temp);
  p.addParameter("http://api.thingspeak.com/update");
  p.run();
}

void refreshExtTemperature() {
  Process p;
  p.begin("curl");
  p.addParameter("-u"); 
  p.addParameter(String(USERNAME)+":"+PASSWORD);
  p.addParameter("-k");
  p.addParameter(String(DOMUS_R2_URL)+"/read_sensor/EXTERNAL");
  p.run();
}


void sendTemp() {
  Process p;
  digitalWrite(Led1, LOW);

  p.begin("curl");
  p.addParameter("-u"); 
  p.addParameter(String(USERNAME)+":"+PASSWORD);
  p.addParameter("-k");
  //p.addParameter("-X POST");
  p.addParameter("--data");
  p.addParameter(String("actual_temperature=")+(int)temperature());
  p.addParameter(String(DOMUS_R2_URL)+"/globals");
  p.run();
  digitalWrite(Led1, HIGH);
}


float temperature() {
  int sensor_val = 0;
  const int count = 5;
  
  for (int i = 0; i < count ; i++) { //average of count readings
    sensor_val = sensor_val + analogRead(A1);
    delay(10);
  }
  return (4.4*sensor_val*100.0/count)/1024.0;
  
//    return (4.4*analogRead(A1)*100.0)/1024.0;
}


