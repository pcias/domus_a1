#define APPLICATION "domusThermoSensor - PCS "
#define VERSION " 1.1 ThingSpeak"
#include <avr/wdt.h>

#include <Bridge.h>
#include <YunServer.h>
#include <YunClient.h>

//temperature logging defines
#include <dht.h>
dht DHT;
#define DHT22_PIN 2

//





const char *key = "7MDUVT8M0INCAWCS";  //ThingSpeak key
const int ARRAY_SIZE = 2;

long int lastSentTS = -99999999999; //millis when last updated ThingSpeak 
long int TS_UPDATE_FREQ = 120000; //2 minutes data



YunServer server;

String last_record;

void setup() {
  Serial.begin(115200);

  Bridge.begin();  
  server.listenOnLocalhost();
  server.begin();

  
  //watchdog - in case we hanged or someone was Morsing too long
  wdt_enable(WDTO_8S);
}





void loop() {
  Serial.println("Booted");
  
  //web service
  YunClient client = server.accept();
  if(client) {
    //read the command
    String command = client.readString();
    command.trim();
    Serial.println("URI:"+command);
    if(command=="sezame") {
          wdt_reset();
    }
    if(command=="read_sensor") {
        int chk = DHT.read22(DHT22_PIN);
        //reading temperature and humidity from the attached DHT22
        client.print("{\"sensor_id\" : \"DHT22M\"");
        switch (chk)
          {
            case DHTLIB_OK:  
                      client.print(", \"status\" : \"OK\""); 
                      break;
            case DHTLIB_ERROR_CHECKSUM: 
                      Serial.print(", \"status\" : \"CHKSUM_ERROR\""); 
                      break;
            case DHTLIB_ERROR_TIMEOUT: 
                      Serial.print(", \"status\" : \"TIMEOUT_ERROR\""); 
                      break;
            default: 
                      (", status : \"UNKN_ERROR\""); 
                      break;
          }
        client.print(", \"temperature\" : ");
        client.print(DHT.temperature);  
        client.print(", \"humidity\" : ");
        client.print(DHT.humidity);  
        client.print(" }");
        client.stop();
    }
    else {
          client.print("{\"status\" : \"CLOSED\"");
          client.print("\"}");
          client.stop();
    }      
  }
  
  //delay(50);
  wdt_reset();  
  
  if(millis() - lastSentTS > TS_UPDATE_FREQ) {
    float data[ARRAY_SIZE];
    int chk = DHT.read22(DHT22_PIN);
    
    if (chk == DHTLIB_OK )
      {
        data[0] = (float)DHT.temperature;
        data[1] = (float)DHT.humidity;
        //workaround for strange readings
        if(data[1] < 100.1) {
          postToThingSpeak(key, data);
        }
        lastSentTS = millis();
      }
  }
}


void postToThingSpeak(String key, float value[]) {
  Process p;
  String cmd = "curl --data \"key="+key;
  for (int i=0;i<ARRAY_SIZE;i++) {
    cmd = cmd + "&field"+ (i+1) + "=" + value[i];
  }
  cmd = cmd + "\" http://api.thingspeak.com/update";
  p.runShellCommand(cmd);
  Console.println(cmd);
  p.close();
}