var redis = require("redis");
var pass = require('pwd');
pass.iterations(10000);


module.exports =function(dbClient) {
    var module ={};

    module.addUser = function(username, password, callback) {
        var userRecord = {}; 
    
        //overwrite the user
        pass.hash(password, function(err, salt, hash){
                        userRecord.salt = salt;
                        userRecord.hash = hash;
                        dbClient.set("domus:passwd:"+username, JSON.stringify(userRecord), function(err,res) { callback(err, res); });
                    });
    }
    
    module.authenticate = function(username,password, callback) {
        
            dbClient.get("domus:passwd:"+username, function (err, userRecord)
            {
                if(userRecord) {
                    pass.hash(password, JSON.parse(userRecord).salt, function(err, hash) {
                        if(err) return(callback(err));
        
                        if(JSON.stringify(hash) == JSON.stringify(JSON.parse(userRecord).hash)) { //password ok
                            callback(null,true);
                        }
                        else    //password wrong
                            callback(null,false);
                    });
                }    
                else { //user not found
                    callback(null,false);
                }    
            });
    
            
        
    }

    return module;
}





