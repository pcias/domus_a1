//cron-like or processing's loop

//not needed anymore as old-style boilerConnect is deprecated in favor of boilerConnectHttp  ---> var net = require('net');
var async = require('async');
var http = require('http');

module.exports = function(dbClient, logger) {
    var module = {};
    var _heating;
    var _global;
    
    var _globalsPrefix = "domus:globals:";

    //prepopulate module properties
    //wonder if this works before methods are called
    //ugliest code ever
    var _boilerIP, _boilerPORT, _boilerWTC, _waterOnCmd, _waterOffCmd, _heatingOnCmd, _heatingOffCmd, _waterTargetCmd, _getHeatingCmd, _getWaterCmd;

    async.parallel([
            function(callback) {
                dbClient.get(_globalsPrefix+"boilerIP", function(err, result) {
                    if(err) throw err;
                    _boilerIP = JSON.parse(result);
                    if(!_boilerIP) throw new Error('No boilerIP defined');
                    callback();
                });
            },
            function(callback) {
                dbClient.get(_globalsPrefix+"boilerPORT", function(err, result) {
                    if(err) throw err;
                    _boilerPORT = JSON.parse(result);
                    if(!_boilerPORT) throw new Error('No boilerPORT defined');
                    callback();
                });
            },                
            function(callback) {
                dbClient.get(_globalsPrefix+"getWaterTempCmd", function(err, result) {
                    if(err) throw err;
                    _boilerWTC = JSON.parse(result);
                    if(!_boilerWTC) throw new Error('No getWaterTempCmd defined');
                    callback();
                });
            },
            function(callback) {
                dbClient.get(_globalsPrefix+"waterOnCmd", function(err, result) {
                    if(err) throw err;
                    _waterOnCmd = JSON.parse(result);
                    if(!_waterOnCmd) throw new Error('No waterOnCmd defined');
                    callback();
                });
            },
            function(callback) {
                dbClient.get(_globalsPrefix+"waterOffCmd", function(err, result) {
                    if(err) throw err;
                    _waterOffCmd = JSON.parse(result);
                    if(!_waterOffCmd) throw new Error('No waterOffCmd defined');
                    callback();
                });
            },
            function(callback) {
                dbClient.get(_globalsPrefix+"heatingOnCmd", function(err, result) {
                    if(err) throw err;
                    _heatingOnCmd = JSON.parse(result);
                    if(!_heatingOnCmd) throw new Error('No waterOnCmd defined');
                    callback();
                });
            },
            function(callback) {
                dbClient.get(_globalsPrefix+"heatingOffCmd", function(err, result) {
                    if(err) throw err;
                    _heatingOffCmd = JSON.parse(result);
                    if(!_heatingOffCmd) throw new Error('No heatingOffCmd defined');
                    callback();
                });
            },
            function(callback) {
                dbClient.get(_globalsPrefix+"waterTargetCmd", function(err, result) {
                    if(err) throw err;
                    _waterTargetCmd = JSON.parse(result);
                    if(!_waterTargetCmd) throw new Error('No waterTargetCmd defined');
                    callback();
                });
            },
            function(callback) {
                dbClient.get(_globalsPrefix+"getHeatingCmd", function(err, result) {
                    if(err) throw err;
                    _getHeatingCmd = JSON.parse(result);
                    if(!_getHeatingCmd) throw new Error('No getHeatingCmd defined');
                    callback();
                });
            },
            function(callback) {
                dbClient.get(_globalsPrefix+"getWaterCmd", function(err, result) {
                    if(err) throw err;
                    _getWaterCmd = JSON.parse(result);
                    if(!_getWaterCmd) throw new Error('No getWaterCmd defined');
                    callback();
                });
            }
            
            
        ]);

    
    
    module.maxextTemp = function(callback) {
        dbClient.get(_globalsPrefix+"maxext", function(err, result)    //maximum external temperature when heating is allowed
            {
                callback(err,JSON.parse(result));
            });
    };    
    
    module.extTemp = function(callback) {
        dbClient.get(_globalsPrefix+"ext_temperature", function(err, result)    //external temperature 
            {    
                callback(err,JSON.parse(result));
            });
    };
    
    module.actTemp = function(callback) {
        dbClient.get(_globalsPrefix+"actual_temperature", function(err, result)
            {
                callback(err,JSON.parse(result));
            });
    };


    //old boilerConnect over ethernet (for Wiznet ethernet shield)
    module.boilerConnect = function(boilerCmd){
        var _boilerResponse = "";

        //byPassing
        return boilerConnectHttp(boilerCmd)
        //rest is bypassed anyway
        
        // console.log("boilerConnect:"+boilerCmd);
        
        // //if testing env
        // if(_boilerIP=="0.0.0.0") {
        //     dbClient.set(_globalsPrefix+"boiler_connection_ok",JSON.stringify(false));
        //     return;
        // }


        
            
        // //otherwise production    
        // var connection = net.connect(_boilerPORT, _boilerIP, function() {
        //     var date = new Date();
        //     console.log(date.toString()+'domus_r2: connection to arduino SUCCESS');
        //     connection.write(boilerCmd+"\n");
        //     dbClient.set(_globalsPrefix+"boiler_connection_ok",JSON.stringify(true));
        // });
        
        // connection.on('error', function() {
        //         var date = new Date();
                
        //         console.log(date.toString()+'domus_r2: connection to arduino ERROR');
        //         dbClient.set(_globalsPrefix+"boiler_connection_ok",JSON.stringify(false));
        //         connection.end();
        // });
        
        // connection.on('data', function(data) {
        //     console.log('domus_r2: arduino response: '+data.toString());
        //     _boilerResponse += data.toString();
            
        //     //use the opportunity to read water temperature
        //     if(_boilerResponse.length >= 9 /* RET:61.26 */) {
        //         switch(boilerCmd) {
        //             case _boilerWTC :
        //                 dbClient.set(_globalsPrefix+"waterTemp", _boilerResponse.substr(4,_boilerResponse.length-6));
        //                 break;
        //             case _getHeatingCmd:
        //                 dbClient.set(_globalsPrefix+"heatingTemp", _boilerResponse.substr(4,_boilerResponse.length-6));
        //                 break;
        //             case _getWaterCmd:
        //                 dbClient.set(_globalsPrefix+"waterHeatingTemp", _boilerResponse.substr(4,_boilerResponse.length-6));    
        //         }
                
        //     }
        //     dbClient.set(_globalsPrefix+"boiler_connection_ok",true);
        //     connection.end();
        // });
    };
    
    //boilerConnectHttp (for Yun-based boiler)
    module.boilerConnectHttp =  function(boilerCmd) {
        var _boilerResponse = "";
        var _boilerCmd = "";
                //if testing env
        if(_boilerIP=="0.0.0.0") {
            dbClient.set(_globalsPrefix+"boiler_connection_ok",JSON.stringify(false));
            return;
        }
        
        //otherwise production 
        
        _boilerCmd = _boilerIP + "/" + boilerCmd;
        console.log("boilerConnectHttp: " + _boilerCmd);
        http.get(_boilerCmd, function(response) {

                    var responseString="";
                    
                    //pass through the response from sensor
                    response.on('data', function(chunk) {
                        responseString += chunk;
                    })
                    
                    response.on('end', function(chunk) {
                        if(chunk) {
                            responseString += chunk;
                        }
                        if(responseString) {
                            //quick and dirty
                            try {
                                dbClient.set(_globalsPrefix+'waterTemp', JSON.parse(responseString).waterTemp);
                                dbClient.set(_globalsPrefix+'heatingTemp', JSON.parse(responseString).heatingTemp);
                                dbClient.set(_globalsPrefix+'waterHeatingTemp', JSON.parse(responseString).waterHeatingTemp);
                                dbClient.set(_globalsPrefix+"boiler_connection_ok",JSON.stringify(true));
                            }
                            catch(e) {
                                console.log('Bad data came from boiler:');
                                console.log(e);
                                dbClient.set(_globalsPrefix+"boiler_connection_ok",JSON.stringify(false));
                            }
                        }
                    })  

        }).on('error', function(e) {
                            console.log(e);
                            dbClient.set(_globalsPrefix+"boiler_connection_ok",JSON.stringify(false));
                        });    
        
        
    }
    
    

    module.heatingOn = function() {
        //_globalsStore.get("heatingOnCmd", function(err,data) { if(!err) { boilerConnectHttp(data); }; } );
        dbClient.set(_globalsPrefix+"heating",true);
        _heating =  true;
        console.log("heating on");
    };

    module.heatingOff = function(){
        //boilerConnect(_globalsStore.get("heatingOffCmd"));
        _heating =  false;
        dbClient.set(_globalsPrefix+"heating",false);
        console.log("heating off");
    };
    
    module.waterOn = function (){
        //boilerConnect(_globalsStore.get("waterOnCmd"));
        _global =  true;
        dbClient.set(_globalsPrefix+"global",true);
        console.log("global on");
    };
    
    module.waterOff = function(){
        //boilerConnect(_globalsStore.get("waterOffCmd"));
        _global =  false;
        dbClient.set(_globalsPrefix+"global",false);
        console.log("global off");
    };


    module.loop = function() {
    
      function actuate(act,st,waterTarget,maxext,ext) {
             //a little hysteresis
             if(act < st - 0.5 && ext < maxext - 0.5) {
                  module.heatingOn();
              }
              else if (act >= st + 0.5 || ext >= maxext + 0.5 ) {
                  module.heatingOff();    
              }
              //needs repeating boiler commands in case arduino resets
            //direct control over 'global' - deprecated by thermostatic fuction  
            //   if(_global) 
            //     module.boilerConnect(_waterOnCmd);
            //   else 
            //     module.boilerConnect(_waterOffCmd);
              if(_heating) 
                module.boilerConnectHttp(_heatingOnCmd);
              else 
                module.boilerConnectHttp(_heatingOffCmd);
                
              setTimeout(function() { 
                            module.boilerConnectHttp(_waterTargetCmd+waterTarget);
                            //console.log("water target pushed: "+_waterTargetCmd+waterTarget);
              }, 100);
              
              setTimeout(function() { module.boilerConnectHttp(_boilerWTC); }, 1000);    
              setTimeout(function() { module.boilerConnectHttp(_getHeatingCmd); }, 2000);    
              setTimeout(function() { module.boilerConnectHttp(_getWaterCmd); }, 3000);    
      }
    
      var _date = new Date();
      var _profile;
      var _act;
      var _maxext;
      var _ext;
      var _ts;
      var _waterTarget;
      var _mt;
      var _st;
      var _data; //read hour data from the database
      
      async.series([
            //read current profile (HOME/AWAY)
            function(callback) {
                dbClient.get(_globalsPrefix+'profile', function(err, result) {
                    if(err) throw err;
                    if(!result) throw new Error('domus no domus:global:profile defined.');
                    _profile = JSON.parse(result);
                    callback();
                });
            },
            //read actual temperature    
            function(callback) {
                module.actTemp(function(err, result) {
                    _act = result;
                    callback();
                });
            },
            //read maximum external temperature when heating allowed
            function(callback) {
                module.maxextTemp(function(err, result) {
                    _maxext = result;
                    callback();
                });
            },
            //read actual external temperature to compare with maxext
            function(callback) {
                module.extTemp(function(err, result) {
                    _ext = result;
                    callback();
                });
            },
            //read hours data
            function(callback) {
                var _hours = _date.getHours();
                dbClient.get('domus:'+_profile+':'+_hours, function(err, result) {
                    if(err) throw err;
                    _data = JSON.parse(result);
                    callback();
                });
            },             
            //now, finally execute actuation if needed
            function(callback) {
                _st =parseInt(_data.temp);
                _waterTarget = parseInt(_data.glob);
                dbClient.set(_globalsPrefix+"web_temperature",_st);
            
                actuate(_act,_st,_waterTarget,_maxext,_ext);
            
                //DEPRECATED water always comes from WEB
                // if(_data.glob) {
                //     module.waterOn();
                // }
                // else {
                //     module.waterOff();
                // }
                
                callback();
            }
            
            
          ]);
      
    };
    
    //othr loop working in slower cycles to log values from waterTemp waterHeatingTemp heatingTemp into log
    module.boilerLogLoop = function() {
        ["waterTemp","waterHeatingTemp","heatingTemp"].forEach(function(entry) {
            dbClient.get(_globalsPrefix+entry, function(err, result) {
                if(!err) {
                    var req={}, res={};
                    req.params = {};
                    req.body = {};
                    req.params.id = entry;
                    req.body.temperature = result;
                    logger.logSensor(req,res);
                }
            });
        });
    };
    
    return module;
};