var redis = require("redis");
var nodemailer = require('nodemailer');
var async = require('async');
var fs = require("fs");
var _ = require("underscore");

var _globalsPrefix = "domus:globals:";

module.exports =function(dbClient) {
    
    var _globalsPrefix = "domus:globals:";
    var _transporter, _emailTemplateFilename, _emailTemplate, _emailRecipients, _address;
    var _transporterOptions;
    
    var _outsideTemp, _outsideHumidity, _insideTemp, _banner, _sunset, _sunrise, _now;
    
    module.email = function() {
        
        async.series([
                        // create reusable transporter object using SMTP transport
                        function(callback) {
                            async.parallel([
                                    function(callback_p) {
                                        dbClient.get(_globalsPrefix+'emailtransporter', function(err,result) { 
                                            if(result) { 
                                                _transporterOptions = JSON.parse(result);
                                                _transporter = nodemailer.createTransport(_transporterOptions); callback_p();
                                            }
                                            });
                                    },
                                    function(callback_p) {
                                        dbClient.get(_globalsPrefix+'emailtemplate', function(err,result) { _emailTemplateFilename = (JSON.parse(result)); callback_p();});
                                    },
                                    function(callback_p) {
                                        dbClient.get(_globalsPrefix+'emailrecipients', function(err,result) { _emailRecipients = (JSON.parse(result)); callback_p();});
                                    },
                                    function(callback_p) {
                                        dbClient.get(_globalsPrefix+'address', function(err,result) { _address = (JSON.parse(result)); callback_p();});
                                    },
                                    function(callback_p) {
                                        dbClient.get(_globalsPrefix+'ext_temperature', function(err,result) { _outsideTemp = (JSON.parse(result)); callback_p();});
                                    },
                                    function(callback_p) {
                                        dbClient.get(_globalsPrefix+'ext_humidity', function(err,result) { _outsideHumidity = (JSON.parse(result)); callback_p();});
                                    },
                                    function(callback_p) {
                                        dbClient.get(_globalsPrefix+'actual_temperature', function(err,result) { _insideTemp = (JSON.parse(result)); callback_p();});
                                    },
                                    function(callback_p) {
                                        dbClient.get(_globalsPrefix+'sunrise', function(err,result) { _sunrise = (JSON.parse(result)); callback_p();});
                                    },
                                    function(callback_p) {
                                        dbClient.get(_globalsPrefix+'sunset', function(err,result) { _sunset = (JSON.parse(result)); callback_p();});
                                    },
                                    function(callback_p) {
                                        dbClient.get('database:news:banner', function(err,result) { _banner = (JSON.parse(result)); callback_p();});
                                    }
                                ], function(err,results) {
                                    callback(); //allow next in series
                                });
                        },
                        //read and render email template and render with underscore template
                        function(callback) {
                             _now = new Date();
                            
                            fs.readFile(_emailTemplateFilename, function (err, data) {
                            if (err) throw err;
                            var s = String(data);
                            
                            _emailTemplate = _.template(s, { 
                                    address : _address, 
                                    hour : _now.toLocaleTimeString(), 
                                    outsideTemp : _outsideTemp,
                                    insideTemp : _insideTemp,
                                    outsideHumidity : _outsideHumidity,
                                    banner : _banner,
                                    sunrise : _sunrise,
                                    sunset : _sunset
                            });
                            callback();
                            });
                        },
                        // setup e-mail data with unicode symbols
                        function(callback) {
                            
                            if(_transporterOptions) {
                                var mailOptions = {
                                    from: _transporterOptions.auth.user, // sender address
                                    to: _emailRecipients, // list of receivers
                                    subject: 'Weather information for '+_address+' at '+ _now.toLocaleString(), // Subject line
                                    //text: 'Hello world ✔', // plaintext body
                                    html: _emailTemplate // html body
                                };
                                // send mail with defined transport object
                                _transporter.sendMail(mailOptions, function(error, info){
                                    if(error){
                                        console.log(error);
                                        console.log(mailOptions);
                                    }
                                    else{
                                        console.log('Message sent: ' + info.response);
                                    }
                                });
                                callback();
                                }
                            }
        ]);
    }

    return module;
}





