Domus_a1
=======

Release 3 (a1) of domusNode X10/heyu Home Automation Server+Web Client

1) Heyu web interface (assumes heyu installed in the system) (lights control) see: http://heyu.tanj.com/
2) Boiler temperature control (arduino - attached 2 relays supported)
3) Arduino - based (e.g. YUN) LCD console for status reporting/yahoo weather data display/manual temperature settings (under development)
4) Door/Gate control (Arduino relays)


#### For domusNode users

This app is rewritten from scratch

Usage
=======
1) Adjust settings in database/settings/
a) AWAY and HOME profiles
b) globals file - in particular:
    "heyuExecutable"    : in case you really use heyu , path to heyu executable
    "x10conf"           : as above, heyu config file path , normally /etc/heyu/x10.conf
    "address"           : your home address. No national characters here. Make sure the address is accepted by weather.yahoo.com, so woeid will be found
    "Yahoo_App_ID"      : you can register as Yahoo developer and get development app id with limited calls for free (for weather forecast)
    "boilerIP" ,                
    "boilerPORT"        : arduino connected to boiler controlling relays,
    "gateURI"           : Arduino YUN address/url that will open the gate, set only the IP, the path is already in line with the arduino code in the domusKerberos folder
    
c) upload json/ministore files to redis databases using ministore2redis.js

2) prepare your hardware:
a) boiler controller - 2 relay board, connected to any arduino w/ethernet (e.g. I use UNO+ethernet) , sketch in /arduino/domusBasementv7/
b) "console" - arduino+LCD+temperature sensor+again network board (so YUN or ethernet) display for 'console' showing system status+weather data (optional), 
    sketch in /arduino/LCD_temp_sensor_etherner_yun_ISR
c) optional extra DHT22 sensor for humidity/temp logging , sketch in /arduino/YUN_DHT22_humidity_google_logger
d) YUN relay - gate lock

3) node app.js (use https://www.npmjs.org/package/forever for best results)

Careful! You need to modify the arduino sketches to update the IP addresses, etc.