#!/usr/bin/env node
//this script migrates json-style ministore to redis database 
//keys are prefixed as specified by argument plus file basename

var optimist = require('optimist')
    .usage('Will load minitore-style into redis database\nUsage: $0 -i [ministore_file] -n [redis_namespace_prefix] -p [port] -a [server_address] ' )
//    .boolean('f')
    .demand('i')
    .default('n','domus')
    .default('p', 6379)
    .default('a','127.0.0.1')
    .boolean('x')
    .boolean('h')
    .alias('h', 'help')
//    .describe('f', 'force (no questions asked - will overwrite database from files)')
    .describe('x', 'push array values as redis lists')
    .describe('h', 'print this help message');
    
if(optimist.argv.h) {
    optimist.showHelp();
    process.exit(0);
}

var path = require('path');
var store = require('ministore')(path.dirname(optimist.argv.i));
var redis = require('redis');

console.log('Uploading from '+optimist.argv.i+ ' to redis' +optimist.argv.a+':'+optimist.argv.p+' prefix '+optimist.argv.n);


var client = redis.createClient(optimist.argv.p, optimist.argv.p, null);
 
client.on("error", function (err) {
        console.log("Error " + err);
    }); 
 
client.on('ready', function() {
    store(path.basename(optimist.argv.i)).forEach(
                    function(key, value) {
                        console.log('-> '+optimist.argv.n+':'+path.basename(optimist.argv.i)+':'+key+'->'+JSON.stringify(value));
                        //Array values pushed as redis lists - not tested
                        if(optimist.argv.x && value instanceof Array) {
                            client.rpush(optimist.argv.n+':'+path.basename(optimist.argv.i)+':'+key, value, function (err,res) {
                              if (err) return console.log('node-redis : ', err)}); // some kind of I/O error
                              console.log('(Array - list) record in');
                        }
                        //everything else pushed as strings
                        else {
                            
                            client.set(optimist.argv.n+':'+path.basename(optimist.argv.i)+':'+key, JSON.stringify(value), function (err,res) {
                              if (err) return console.log('node-redis : ', err)}); // some kind of I/O error
                              console.log('record in');
                            }    
                        }
                        );
    client.quit();
    }
);



