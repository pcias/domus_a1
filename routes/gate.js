
/*
 * REST gate opener
 */


var http = require('http');

module.exports = function gate(dbClient) {
    
    var module = {};
    
    //opens the gate 
    module.open = function(req, res){
        dbClient.get('domus:globals:gateURI', function(err,URI){
            if(!URI) {
                res.send("missing gateURI in globals.db. Check config");
                console.log("missing gateURI in globals.db. Check config");
                return;
            }
                
            
            http.get(URI, function(response) {
                            console.log("gate open URI sent");
                            res.send('{"status" : "ok"}');
                        }).on('error', function(e) {
                            console.log(e);
                            res.send(e);
                        });    
            });
        
    };

    return module;
};
