
/*
 * REST hours setting.
 */

//redis - done

var async = require('async');

module.exports =function(dbClient) {
            var module = {};
            
            //expected to receive object
             module.put = function(req, res){
            
                    var profilePrefix;
                    
                    async.series([
                        function(callback) {
                                dbClient.get("domus:globals:profile", function(err,result) {
                                    profilePrefix = JSON.parse(result);
                                    callback(null,JSON.parse(result));        
                                })
                            },
                        
                        function(callback) {
                            var newObj = {};    
                            newObj['temp'] = req.body.temp;
                            newObj['glob'] = req.body.glob;
                    
                            dbClient.set("domus:"+profilePrefix+":"+req.params.hour,JSON.stringify(newObj));
                            callback(null, newObj);
                            }
                    ],
                    function(err, results){
                        res.send(results);
                    });
            };

            module.get = function(req, res){

                    var profilePrefix;
                    var hours = [];
                    
                    async.series([
                            function(callback) {
                                dbClient.get("domus:globals:profile", function(err,result) {
                                    profilePrefix = JSON.parse(result);
                                    callback();        
                                })
                            },
                            
                            function(callback) {
                                async.eachSeries([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23], 
                                    function(hour,cb2) {
                                        dbClient.get("domus:"+profilePrefix+":"+hour,
                                                    function (err, hourData) {
                                                        if (err) cb2(err);
                                                        var newObj = {};
                                                        newObj.hour = hour; 
                                                        newObj.temp = JSON.parse(hourData).temp; 
                                                        newObj.glob = JSON.parse(hourData).glob; 
                                                        hours.push(newObj);
                                                        cb2();
                                                    })}, 
                                    function(err) { 
                                        if (err) { throw err; }
                                        callback();
                                    })
                                }
                            ],
                            
                            function(err) {
                                res.json(hours);
                            }
                        
                        );
                        
                    }
                    

            
            return module;

}