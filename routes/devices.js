
/*
 * REST heyu interface
 */



module.exports = function (dbClient) {

    var module={};


    //expected to receive object
    module.put = function(req, res){
                var heyu = require("heyu");
                
                heyu.execHeyu(req.params.id,req.body.command,req.body.comparam, function() { res.send("Heyu executed"); });
    };
    
    
    module.get = function(req, res){
        var heyu = require("heyu");
                
        dbClient.get("domus:globals:x10conf", function (err,result) {
            if(err) throw err;
            if(result)
                heyu.readConfigFile(JSON.parse(result), function() {
                    res.json(heyu.x10grouped);
                });
        });        
    };
    
    return module;

}