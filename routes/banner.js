
/*
 * banner provider for arduino LCD
 * and now all weather related functions
 */

var async = require('async');

module.exports = function (dbClient) {

        var module = {};

        var _tsession = require("temboo/core/temboosession");
        var _session;
        
        
        
        var _newsPrefix = "domus:news:";
        var _globalsPrefix = "domus:globals:";
        var _weatherPrefix = "domus:weather:";
        
        var _banners=[];
        var _waterTemp;
        
        //xml parsing required to read yahoo data
        var _select = require('xpath.js')
            , dom = require('xmldom').DOMParser;

        var _i = 0; //round robin
        
        var _weather = require('openweather-node');
        var _weatherData = {};
        var _sunrise, _sunset;
        var _condition_code;

        //sooner or later - populate
        dbClient.get(_globalsPrefix+'sunrise', function(err, result) { _sunrise = JSON.parse(result); });
        dbClient.get(_globalsPrefix+'sunset', function(err, result) { _sunset = JSON.parse(result); });
        dbClient.get(_globalsPrefix+'current_weather_condition', function(err, result) { _condition_code = JSON.parse(result); });
        
        //returns banner
        module.get = function(req, res){
            async.parallel([
                    function(callback) {
                        dbClient.get(_globalsPrefix+"waterTemp", function(err, res){ 
                            _waterTemp =  JSON.parse(res);
                            callback();
                        });       
                    },
                    function(callback) {
                        dbClient.get(_newsPrefix+"banner", function(err, res){ 
                            _banners =  JSON.parse(res);
                            callback();
                        })
                    }
                ],
                function(){
                    //and add here heating water temperature, too
                    _banners.push('Heating:' + _waterTemp);
                    res.send(_banners[ (_i++) % _banners.length ]);                    
                });
        };
        
        
        //returns full forecast (for jumbotron)
        module.getFull = function(req,res) {
            async.parallel([
                    function(callback) {
                        dbClient.get(_newsPrefix+"banner", function(err, res){ 
                            _banners =  JSON.parse(res);
                            callback();
                        })
                    }
                ],
                function(){
                    res.send(_banners.slice(1,4));                    
                });            
            
        }
        
        //returns weather photo, if set in the database under domus:weather, depanding on time of the day and banner[0] - actual weather from Yahoo
        module.getWeatherPhoto = function(req,res) {

            var date = new Date();
            var hours = date.getHours();
            var sunriseHour = parseInt(_sunrise.substr(0, _sunrise.indexOf(':')));
            var sunsetHour = parseInt(_sunset.substr(0, _sunset.indexOf(':')));
            var photo;
            
            async.series([
                //first try match photo from codition code
                function(callback) {
                    dbClient.get(_weatherPrefix+_condition_code, function(err, result) {
                        if(result) {
                            photo = JSON.parse(result).photo;
                        }
                        callback();
                    })
                },
                //if not successfull, fallback to simplicity
                function(callback){
                    if(!photo) { //if previous step not successful
                        if(hours < sunriseHour || hours > sunsetHour) {
                            photo = "/photo_cache/night_clean.jpg";
                        }
                        else {
                            photo = "/photo_cache/day_clean.jpg";
                        }
                    }
                    callback();
                }
                
                ],
                function() {
                    dbClient.set(_globalsPrefix+'weather_photo', JSON.stringify(photo));  
                    res.send(JSON.stringify(photo));
                });
            
            
        }
        
        //populate - direct Yahoo call (no Temboo required)
        module.populate = function(req,res) {
            //jest problem - yahoo weather wymaga teraz OAuth
            var _params = {
                 location : '', 
                 unit     : 'c', // Celcius(default, "c") or Fahrenheit ("f")
                 appid    : '',  //will read from the db
                 logging  : true //Debug info or not
            }
            
            async.series([
                function(callback) {
                                //read the address from the database
                                dbClient.get(_globalsPrefix+"address", function(err, data){ 
                                _params.location =  JSON.parse(data);
                                callback();
                                });                       
                           },
                function(callback) {
                                //read OPW APP ID
                                dbClient.get(_globalsPrefix+"openweather_App_ID", function(err, data){ 
                                _params.appid =  JSON.parse(data);
                                callback();
                                });                       
                  }
                // commented out since Yahoo now requires authentication. Bugger
                   ,           
                 function(callback) {
                        var _weatherConditionText;
                        var bannerArray;
                        
                        _weather.setAPPID(_params.appid);
                        _weather.setCulture("pl");
                        //set the forecast type 
                        _weather.setForecastType(""); //or "" for 3 hours forecast
                        
                        _weather.forecast(_params.location, function(err, data) {
                            if(err) console.log(err);
                        	else    {
                        	    bannerArray = [];
                        	    dbClient.set(_globalsPrefix+'weatherData',JSON.stringify(data));
                        	    i = 0
                        	    
                        	    if(data.values.list.constructor === Array) {
    		                        data.values.list.forEach(function(aData) {
    			                    //same weather.forecast
    			                        _weatherConditionText = aData.dt_txt.substr(11,5) + " " + aData.weather[0].description
    			                        console.log(_weatherConditionText);
    			                        bannerArray[i++] = _weatherConditionText;
    	                            })
    	                            dbClient.set(_newsPrefix+'banner',JSON.stringify(bannerArray));
                        	    }
                        	    else {
                        	        console.log(data);
                        	    }
	                        }
	                        if(res)
                                 res.json(bannerArray);
                        
                            callback();
                        });    
                
                //old Yahoo
                //         _weatherData =  data;
                //         var bannerArray =  [];
                        
                //         //new
                //         dbClient.set(_globalsPrefix+'weatherData',JSON.stringify(data));
                        
                //         //legacy
                //         dbClient.set(_globalsPrefix+'outside_temperature', data.channel.item["yweather:condition"]["@"]["temp"]);
                //         dbClient.set(_globalsPrefix+'current_weather', data.channel.item["yweather:condition"]["@"]["text"]);
                        
                //         _condition_code =  data.channel.item["yweather:condition"]["@"]["code"];
                //         dbClient.set(_globalsPrefix+'current_weather_condition', _condition_code);
                        
                        
                //         _sunrise = convertTo24Hour(data.channel["yweather:astronomy"]["@"].sunrise);
                //         dbClient.set(_globalsPrefix+'sunrise', JSON.stringify(_sunrise));
                //         _sunset = convertTo24Hour(data.channel["yweather:astronomy"]["@"].sunset);
                //         dbClient.set(_globalsPrefix+'sunset', JSON.stringify(_sunset));
                        
                //         bannerArray[0] = "Outside "+data.channel.item["yweather:condition"]["@"]["temp"]+" C";
                //         bannerArray[1] = data.channel.item["yweather:condition"]["@"]["text"];
                //         var index = 2;
                //         for(var i = 1; i <= 2 ; i++) {
                //             var day = data.channel.item["yweather:forecast"][i]["@"]["day"];
                //             var low = data.channel.item["yweather:forecast"][i]["@"]["low"];
                //             var high = data.channel.item["yweather:forecast"][i]["@"]["high"];
                //             var text = data.channel.item["yweather:forecast"][i]["@"]["text"];
                //             bannerArray[index++] = day+' '+low+'-'+high+' '+text;
                //         }
                //         dbClient.set(_newsPrefix+'banner',JSON.stringify(bannerArray));
                //         if(res)
                //             res.json(bannerArray);
                        
                //         callback();
                //     });
                 }           
                           
                ])
        }
        
        //LEGACY: read Temboo Yahoo- weather and populate banner
        module.populateLegacy = function(req, res) {
  
                //helper function for temboo
                var _tuser;
                var _tapp;
                var _tkey;
                var _address;
                var _return = "OK";
                
                function _init(callback) {
                    async.parallel([
                           function(callback2) {
                                dbClient.get(_globalsPrefix+"temboo_USER", function(err, res){ 
                                _tuser =  JSON.parse(res);
                                callback2();
                            });       
                           },
                           function(callback2) {
                                dbClient.get(_globalsPrefix+"temboo_APPLICATION_NAME", function(err, res){ 
                                _tapp =  JSON.parse(res);
                                callback2();
                            });                       
                           },
                           function(callback2) {
                                dbClient.get(_globalsPrefix+"temboo_APPKEY", function(err, res){ 
                                _tkey =  JSON.parse(res);
                                callback2();
                            });                       
                           },
                           function(callback2) {
                                dbClient.get(_globalsPrefix+"address", function(err, res){ 
                                _address =  JSON.parse(res);
                                callback2();
                            });                       
                           }
        
                        ],
                        function() {
                            _session = new _tsession.TembooSession(_tuser, _tapp, _tkey);
                            callback();
                        });    
                }
  
            //tembo call helper - used then in async
                function _tembooCall(callback) {
                            //Yahoo part
                            var Yahoo = require("temboo/Library/Yahoo/Weather");
                            var getWeatherByAddressChoreo = new Yahoo.GetWeatherByAddress(_session);
                        
                            // Instantiate and populate the input set for the choreo
                            var getWeatherByAddressInputs = getWeatherByAddressChoreo.newInputSet();
                        
                            
                            // Set inputs
                            getWeatherByAddressInputs.set_Units("c");
                            getWeatherByAddressInputs.set_Address(_address);
                        
                            // Run the choreo, specifying success and error callback handlers
                            getWeatherByAddressChoreo.execute(
                                getWeatherByAddressInputs,
                                function(resultsxml){ 
                                        //var banners = bannerStore.get('banner');
                                        var bannerArray =  [];
                                    
                                        var doc = new dom().parseFromString(resultsxml.get_Response()); 
                                        //temperature
                                        var nodes = _select(doc, "/rss/channel/item/yweather:condition/@temp");
                                        if(nodes) {
                                            bannerArray[0] = "Outside "+nodes[0].nodeValue+" C";
                                            dbClient.set(_globalsPrefix+'outside_temperature',JSON.stringify(parseInt(nodes[0].nodeValue)));
                                        }    
                                        //weather description
                                        nodes = _select(doc, "/rss/channel/item/yweather:condition/@text");
                                        if(nodes) {
                                            bannerArray[1] = nodes[0].nodeValue;
                                            dbClient.set(_globalsPrefix+'current_weather',nodes[0].nodeValue);
                                        }    
                                        //weather condition code
                                        nodes = _select(doc, "/rss/channel/item/yweather:condition/@code");
                                        if(nodes) {
                                            _condition_code = nodes[0].nodeValue;
                                            dbClient.set(_globalsPrefix+'current_weather_condition',JSON.stringify(nodes[0].nodeValue));
                                        }
                                        //sunrise & sunset
                                        nodes = _select(doc, "/rss/channel/yweather:astronomy/@sunrise");
                                        if(nodes) {
                                            _sunrise = convertTo24Hour(nodes[0].nodeValue);
                                            dbClient.set(_globalsPrefix+'sunrise', JSON.stringify(_sunrise));
                                        }
                                        nodes = _select(doc, "/rss/channel/yweather:astronomy/@sunset");
                                        if(nodes) {
                                            _sunset = convertTo24Hour(nodes[0].nodeValue);
                                            dbClient.set(_globalsPrefix+'sunset', JSON.stringify(_sunset));
                                        }
                                        //forecast for today
                                        var nodes_day = _select(doc,"/rss/channel/item/yweather:forecast/@day");
                                        var nodes_low = _select(doc,"/rss/channel/item/yweather:forecast/@low");
                                        var nodes_high= _select(doc,"/rss/channel/item/yweather:forecast/@high");
                                        var nodes_text = _select(doc,"/rss/channel/item/yweather:forecast/@text");
                                        var index = 2;
                                        for(var i = nodes_day.length-1; /*i>=0*/ i >= nodes_day.length-2 ; i--) {
                                            var day = nodes_day[i].nodeValue;
                                            var low = nodes_low[i].nodeValue;
                                            var high = nodes_high[i].nodeValue;
                                            var text = nodes_text[i].nodeValue;
                                            bannerArray[index++] = day+' '+low+'-'+high+' '+text;
                                        }
                        
                                        
                                        dbClient.set(_newsPrefix+'banner',JSON.stringify(bannerArray));
                                        res.json(bannerArray);
                                     
                                        },
                                function(error){ _return = error.message ; console.log(error.type); console.log(error.message); callback()}
                            );                    
                }
                
            
            
            //actual execution code here
            //Temboo initialization
            async.series([ _init, _tembooCall, function() { res.send(_return); }]);
            
            
        };
        
        return module;
};


function convertTo24Hour(time) {
    var hours = parseInt(time.substr(0, 2));
    if(time.indexOf('am') != -1 && hours == 12) {
        time = time.replace('12', '0');
    }
    if(time.indexOf('pm')  != -1 && hours < 12) {
        time = time.replace(hours, (hours + 12));
    }
    return time.replace(/(am|pm)/, '');
}