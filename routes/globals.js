
/*
 * REST globals setting.
 */

module.exports = function(dbClient) {
    var module = {};    
    var _prefix = "domus:globals:"
 
    //expected to object-value for REST :id key 
    module.put = function(req, res){
                console.log(req.body);
                dbClient.set(_prefix+req.params.id,JSON.stringify(req.body[req.params.id]), function() { res.send(req.body); });   //ugly
    };
    
    //this one is mostly for legacy ARDUINO controller that POSTS just this way
    module.post = function(req,res) {
            var date = new Date();
            var response = false; //assume failure

            for(var key in req.body) {
                dbClient.set(_prefix+key,JSON.stringify(req.body[key]));    
            };
            dbClient.set(_prefix+'timestamp',JSON.stringify(date));
            
            res.json(response);
            return;  
    };
    
    
    module.get = function(req, res){
                    if(req.params.id !== "temboo_APPKEY")
                        dbClient.get(_prefix+req.params.id, function(err,result) { 
                            if(err) 
                                res.send("FALSE"); 
                            else 
                                res.send(result)})
                    else
                        res.send("Forbidden");
    };
    
    return module;

}