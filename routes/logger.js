/*
 *  logger (neverending)
 */

//ministore 
//var Store = require('ministore')('./database/log');
//var logStore = Store('log');
//var Spreadsheet = require('edit-google-spreadsheet');
var http = require('http');
var async = require('async');
var _ = require("underscore");
var csv = require('to-csv');

//helper function
var flattenLogs = function(ob) {
	var toReturn = [];
	
	for (var log in ob) {
	    for(var logItem in ob[log].logData) {
	        var line = {};
	        line.sensor = ob[log].logName;
	        line.time=logItem;
	        for (var prop in ob[log].logData[logItem]) {
	            line[prop] = ob[log].logData[logItem][prop];
	        }
	        toReturn.push(line);
	    }
	}
	
	return toReturn;
};


module.exports = function logger(dbClient) {
    var module = {};

    var _sensorPrefix = "domus:sensors:";
    var _logPrefix = "domus:log:";
    var _globalsPrefix = "domus:globals:";
    var _lastDate = new Date();
    
    //returns response {sensor_id : "DHT22", status : "OK", temperature : 14.90, humidity : 67.00 }
    //reads sensor from http and logs (for passive sensors)
    module.read_sensor = function(req,res) {
        if(req.params.id) {
            //read sensor URL from database
            dbClient.get(_sensorPrefix+req.params.id, function(err, sensor) {
                //call URL
                http.get(JSON.parse(sensor).URL, function(response) {
                    var responseString="";
                    
                    //pass through the response from sensor
                    response.on('data', function(chunk) {
                        if(res) {
                            res.write(chunk);
                        }
                        responseString += chunk;
                    })
                    
                    response.on('end', function(chunk) {
                        if(res) {
                            res.end(chunk);
                        }
                        if(chunk) {
                            responseString += chunk;
                        }
                        if(responseString) {
                            //quick and dirty
                            if(req.params.id == 'EXTERNAL') {
                                dbClient.set(_globalsPrefix+'ext_temperature', JSON.parse(responseString).temperature);
                                dbClient.set(_globalsPrefix+'ext_humidity', JSON.parse(responseString).humidity);
                            }
                            //logging
                            var obj = JSON.parse(responseString);
                            obj.date = new Date();
                            //log no more often than 5 minutes - to preserve database
                            if(obj.date.getTime() - _lastDate.getTime() > 300000) {
                                dbClient.rpush(_logPrefix+req.params.id,JSON.stringify(obj));
                                _lastDate = obj.date;
                            }
                        }
                    })                    
                    
                }).on('error', function(e) {
                    console.log(e);
                    if(res) {
                        res.send(e);
                    }
                });    
            });   
        }
    }
    
    //only logs sensor (for active sensors) 
    module.logSensor = function (req,res) {
        if(req.params.id) {
            req.body.date = new Date();
            dbClient.rpush(_logPrefix+req.params.id,JSON.stringify(req.body));
            console.log("Log: "+_logPrefix+req.params.id+JSON.stringify(req.body));
        }
    }
    
    //read 1970 millis
    module.millis = function(req,res) {
        res.send(200, String(new Date().getTime()));
    }
    
    //read local time
    module.localtime = function(req,res) {
        res.send(200, new Date().toLocaleTimeString());
    }
    
    //read log for all sensors in a table , accepts the slice parameters begin end - with their '-1' semantics
    //as well as dateStart and dateStop for further filtering the output
    //averages - averages per 'Y' 'M' 'D' 'H' (null) - no averages
    //if req.query.format is set to "csv"-> then returns in csv format 
    module.readLog = function(req,res) {
        var _logs=[];
        var _start = 0;
        var _stop = -1;
        var _dateStart = "1900";
        var _dateStop = "9999";
        
        if(req.query.start)
            _start = req.query.start;
        if(req.query.stop)
            _stop = req.query.stop;
        if(req.query.dateStart)
            _dateStart = JSON.parse(req.query.dateStart);
        if(req.query.dateStop)
            _dateStop = JSON.parse(req.query.dateStop);
        
        dbClient.keys(_logPrefix+'*', function (err, keys) {
          if (err) 
            return console.log(err);
        
        
          async.each(keys, function(key, callback) {
            dbClient.lrange(key, _start, _stop,  function(err,data) {
                            var _newObj = {};
                            _newObj.logName = key;
                            _newObj.logData = _.filter(data, function(element) {
                                            var _parsed = JSON.parse(element);
                                            return _parsed.date >= _dateStart && _parsed.date <= _dateStop;  });
                            //producing Yearly Monthly Daily, Hourly averages instead of full dump , if requested
                            var _lastChar = 1000;
                            switch(req.query.averages) {
                                case 'Y':
                                    _lastChar = 4;
                                    break;
                                case 'M':
                                    _lastChar = 7;
                                    break;
                                case 'D':
                                    _lastChar = 10;
                                    break;
                                case 'H':
                                    _lastChar = 13;
                                    break;
                            }      
                            //group by specific time period based on date substring
                            _newObj.logData =  _.groupBy(_newObj.logData, function(element) {
                                    var _parsed  = JSON.parse(element);
                                    return _parsed.date.substring(0, _lastChar);
                                });
                            
                            //per each group reduce the readings to averages    
                            _.each(_newObj.logData, function (value, key) { 
                                var _temp = {}; 
                                var _denominator = value.length;  //value is an array of readings to be averaged
                                
                                _denominator = _denominator !== 0 ? _denominator : 0; 
                                
                                _temp =  _.reduce(value, function(memo,item,index,list) {
                                    var _ret = {};
                                    var _parsedItem = JSON.parse(item);
                                    for (var key in _parsedItem) {
                                         if (_parsedItem.hasOwnProperty(key)) {
                                                if(isNaN(_parsedItem[key])) {
                                                    _ret[key] = _parsedItem[key];  //if not a number - copy over
                                                }  
                                                else {
                                                    _ret[key] = parseFloat(_parsedItem[key])/_denominator + memo[key]; //if number summarize
                                                }
                                             }
                                    }
                                    return _ret;
                                    }, {"temperature" : 0 , "humidity" : 0});
                                    _newObj.logData[key] = _temp;
                            });
                                
                            
                            _logs.push(_newObj);
                            callback();
                        });
          },
          function(err) {
              if(err) {
                res.json("false");
              }
              else {
                  if(req.query.format==="csv") {
                    res.send(csv(flattenLogs(_logs)));
                  }
                  else {    
                    res.json(_logs);
                  }
              }
          });
        }
        );

    }
    
  
    
    return module;
}


        

